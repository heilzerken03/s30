// 2. Count Operator
db.fruits.aggregate([
		{
			$match: { onSale: true}
		},

		{
			$group: {
				_id: "$supplier_id",
				count: { $sum: "$stock" },
			}
		},

		{
			$sort: { count: 1 }
		}
])

// 3. Count Operator / Greater than or Equal Operator
db.fruits.aggregate([
		{
			$match: { stock: {$gte: 20}}
		},
		{
			$group: {
				_id: "$supplier_id",
				count: { $sum: "$stock" },
			}
                       
		},
		{
			$sort: { total: 1 }
		}
])

// 4. Average Operator
db.fruits.aggregate([
		{
			$match: { onSale: true }
		},
		{
			$group: {
				_id: "$supplier_id",
           		avgPrice: { $avg: "$price" }
			}
                       
		},
])

// 5. Max Operator
db.fruits.aggregate([
		{
			$group: {
				_id: "$supplier_id",
               	maxPrice: { $max: "$price" }
		}
	},
])

// 6. Minimum Operator
db.fruits.aggregate([
		{
			$group: {
				_id: "$supplier_id",
               	minPrice: { $min: "$price" }
		}
	},
])